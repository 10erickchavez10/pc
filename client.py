import socket  as s

varSock = s.socket(s.AF_INET, s.SOCK_STREAM)

varHost = s.gethostname()

port = 7777
#Conecta
varSock.connect((varHost,port))

#Cantidad de byts que puede recibir un msj
msg = varSock.recv(1024)
#Cierra
varSock.close()
#Muestra el mensaje
print(msg.decode('ascii'))


