# Librerias necesarias para el funcionamiento de la app

import math
import socket
from sys import argv

#checa que este bien
def chequeo(a,p,debug):
    print ("Checando.")
    pls = sig(p)
    res = a
    while (a!=0 and sig(a)>=pls):
        if debug==1:
            print (bin(a))
            print (bin(p<<(sig(a)-pls)))
            print ("---------------------------------")
        a^=p<<(sig(a)-pls)
        if debug==1:
            print bin(a)
            print""
    if debug==1:
        print "Resultado: ",bin(a)
    if a==0:
        return 1
    else:
        return 0

#Division
def divide(a,p,debug):
    res=a
    pls=sig(p)
    a<<=pls
    while sig(a)>=pls:
        if debug==1:
            print""
            print bin(a)
            print bin(p<<(sig(a)-pls))
            print "---------------------------------"
        a^=p<<(sig(a)-pls)
        if debug==1:
            print bin(a)
            print""
    res<<=pls
    res^=a #xor
    if debug==1:
        print "Resultado: ",res
    return res

#String a binario
def stringbin(a):
    res=0
    for i in a:
        res<<=7 
        res^=ord(i)
    return res #retorno del resultado

#Bit mas significativo
def sig(a):
    return int(math.log(a,10)/math.log(2,10))

def ruido(a,x,debug):
    if debug==1:
        print ("Generando ruido...")
        print ("Original:  ",bin(a))
        print ("Con Ruido: ",bin(a|x))
    a|=x
    return a

host = argv[1]
rol = argv[2]

if host=="w":#host wifi
    hstip=raw_input("Ingrese la ip del host: ")
if host=="l":#host local
    hstip=socket.gethostname()
    

port, varSock = 1212, socket.socket()
debug,order = 1,11111

if rol == "transmisor":
    varSock.connect((hstip, port))
    mensaje=raw_input("Escriba su mensaje: ")
    print ("Mensaje: ",mensaje)
    mensaje=stringbin(mensaje)
    print ("Mensaje en binario: ",bin(mensaje))
    mensaje=divide(mensaje,int(order),debug)
    op=str(raw_input("Quieres poner interferencia? (s->Si, n->No) :"))
    if op=='S' or op == 's':
        ns=int(raw_input("Ingrese interferencia (1|0): "))
        mensaje=ruido(mensaje,ns,debug)
    print ("ENVIANDO")
    varSock.send(str(mensaje))
if rol=="receptor":
    print ("Esperando")
    varSock.bind((hstip,port))
    varSock.listen(3)
    c,addr=varSock.accept()
    data=int(c.recv(4069))
    print data
    print bin(data)
    if chequeo(data,int(order),debug)==1:
        print ("Recibi sin errores!!")
    else:
        print ("Recibi con errores!!")
varSock.close()

#python crc.py l (local) receptor
#python crc.py l (local) transmisor
#python crc.py w (wifi) receptor
#python crc.py w (wifi) transmisor