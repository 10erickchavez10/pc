# Librerias necesarias para el funcionamiento de la app
import math
import socket
import atexit


# Metodo para efectuar la division
def divide(a,p,debug):
    res=a
    pls=sig(p)
    a<<=pls
    while sig(a)>=pls:
        if debug==1:
            print bin(a)
            print bin( p<<( sig( a ) - pls ) )
            print "---------------------------------------------------"
        a^=p<<(sig(a)-pls)
        if debug==1:
            print bin(a),"\n"
    res<<=pls
    res^=a #xor
    if debug==1:
        print "Resultado: ",res
    return res

#String a binario
def stringbin(a):
    res=0
    for i in a:
        res<<=7 #Corrimiento de 7 espacios a la izquierda
        res^=ord(i) #Combinacion de la secuencia de bits
    return res #resultado

#bit mas significativo
def sig(a):
    return int(math.log(a,10)/math.log(2,10))

#Inserta el ruido
def ruido(a,x,debug):
    if debug==1:
        print "Original:  ",bin(a)
        print "Con interferencia: ",bin(a|x)
    a|=x
    return a
#Inicializa el socket
varSock = socket.socket()

def main():
    #Decide si es wifi o lan
    print "w -> Wifi, l-> lan"
    host = raw_input(">")
    if host=="w":#host wifi
        host=raw_input("Ingresa la ip del host: ")
    if host=="l":#host local
        host=socket.gethostname()

    #Ingresa el puerto
    debug = 1
    port = int(raw_input("Ingresa el puerto:    "))
    #poly = 1010
    poly = int(raw_input("Ingresa el polinomio: "))

    varSock.connect((host, port))
    #Ingresa el mensaje
    mensaje=raw_input("Escribe tu mensaje: ")
    print "Mensaje: ",mensaje
    #Convierte el mensaje en binario
    mensaje=stringbin(mensaje)
    print "Mensaje en binario: ",bin(mensaje)
    #Ejecuta la division con el poli
    mensaje=divide(mensaje,int(poly),debug)
    #Pone interferencia
    op=str(raw_input("Quieres poner interferencia (S|s = si) "))
    if op=='S' or op == 's':
        rdo=int(raw_input("Ingrese interferencia (1|0): "))
        mensaje=ruido(rdo,debug,mensaje)
    #Envia el mensaje
    print "ENVIANDO"
    varSock.send(str(mensaje))
    



def cierra():
    varSock.close()
    
atexit.register(cierra)

if __name__ == "__main__":
    main()