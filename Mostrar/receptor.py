# Librerias necesarias para el funcionamiento de la app
import math
import socket
import binascii
import atexit


# << n = Recorre n a la izquierda
# ^= XOR
# |= OR

#Checa el mensaje con el polinomio y va mostrandolo
#A = MENSAJE
#P = Polinomio
#debug = PASO A PASO
def chequeo(a,p,debug):
    print ("Checando.")
    res = a
    pls = sig(p)
    while (a!=0 and sig(a)>=pls):#Empieza while
        if debug==1:
            print (bin(a))
            print (bin(p<<(sig(a)-pls)))
            print ("---------------------------------")
        a^=p<<(sig(a)-pls)#xor
        if debug==1:
            print bin(a)
            print ("")
    #Termina while
    if debug==1:
        print "Resultado: ",bin(a)
    if a==0:
        return 0
    else:
        return 1

#Bit mas significativo
def sig(a):
    return int(math.log(a,10)/math.log(2,10))

#Saca la ip de la propia maquina (receptor)
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
print(s.getsockname()[0])
s.close()

varSock = socket.socket()

def main():
    #Decide si es wifi o lan
    print "w -> Wifi, l-> lan"
    host = raw_input(">")
    if host=="w":#host wifi
        host=raw_input("Ingresa la ip del host: ")
    if host=="l":#host local
        host=socket.gethostname()
    #varSock = socket.socket()
    debug = 1
    poly = int(raw_input("Ingrese el polinomio: "))
    #Ingresa el puerto
    puerto = int(raw_input(">Ingresa el puerto: "))
    #Esperando el mensaje
    print ("Esperando")
    #Enlaza ip y puerto
    varSock.bind((host,puerto))
    #Cantidad de ecuchados
    varSock.listen(1)
    c,addr=varSock.accept()
    #Recibe el mensaje
    msj=int(c.recv(4069))
    #Imprime el mensaje original y en binario
    print "MENSAJE ORIGINAL:   ", msj
    print "MENSAJE EN BINARIO: ", bin(msj)
    #Checa que este bien el mensaje recibido con el poli
    if chequeo(msj,int(poly),debug)==0:
        print ("sin errores")
    else:
        print ("con errores")
    varSock.close()

def cierra():
    varSock.close()
    
atexit.register(cierra)

if __name__ == "__main__":
    main()
#python crc.py l (local) receptor
#python crc.py l (local) transmisor
#python crc.py w (wifi) receptor
#python crc.py w (wifi) transmisor