# Librerias necesarias para el funcionamiento de la app

import struct
import math
import socket 
import binascii

from sys import argv


#Obtiene el polinomio
def poly(n,debug):
    res=0
    while n>0:
        res+=2**n
        n-=1
    if debug==1:
        print "Polinomio: ",bin(res)
    return res

#Metodo que cambia un valor de tipo string a binario
def strbin(a):
    res=0;#variable donde se almacena el resultado
        #Ciclo for donde se pasa por todo el arreglo de chars (variable de tipo string)
    for i in a:
        res<<=7 #Corrimiento de 7 espacios a la izquierda
        res^=ord(i) #Combinacion de la secuencia de bits
    return res #retorno del resultado

#Retorna la posicion  del bit mas significativo
def sig(a):
    return int(math.log(a,10)/math.log(2,10))

#Metodo para efectuar el chequeo
def check(a,p,debug):
    print "Revisando secuencia..."
    res=a
    pls=sig(p)
    while (a!=0 and sig(a)>=pls):
        if debug==1:
            print""
            print bin(a)
            print bin(p<<(sig(a)-pls))
            print "---------------------------------"
        a^=p<<(sig(a)-pls)
        if debug==1:
            print bin(a)
            print""
    if debug==1:
        print "Resultado de Revision: ",bin(a)
    if a==0:
        return 1
    else:
        return 0


#Obtencion de variables
#hstloc
#l -> local
#r -> remoto
#hstloc=argv[1]
#rol
#rx->receptor
#tx->transmisor
#rol=argv[2]

hstip=socket.gethostname()
#Descomentar estas linea para elegir el polinomio
order=raw_input("Enter the poly: ")
port, s = 2121, socket.socket()

print "waiting for data..."
s.bind((hstip,port))
s.listen(1)
#c,addr=s.accept()
c,addr=s.accept()
data=int(c.recv(4069))
print data," SOY DATA"
print bin(data)

if check(data,int(order),1)==1:
    print "Informacion Recibida sin errores!!"
else:
    print "Informacion Recibida con errores!!"

s.close()
